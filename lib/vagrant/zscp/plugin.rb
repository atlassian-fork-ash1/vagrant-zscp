require "vagrant"

module Vagrant
  module Zscp
    # This plugin implements zSCP synced folders.
    # It will also compress folders into a targz file to reduce back and forth
    # communication
    #
    class Plugin < Vagrant.plugin("2")
      name "zSCP synced folders"
      description <<-EOF
      The zSCP synced folders plugin enables you to use SCP as a synced folder
      implementation.
      EOF

      command("zscp", primary: false) do
        require_relative "command/zscp"
        Command::Rsync
      end

      synced_folder("zscp", 6) do
        require_relative "synced_folder"
        SyncedFolder
      end
    end
  end
end
