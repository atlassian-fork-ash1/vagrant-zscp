require "log4r"

require_relative "helper"

module Vagrant
  module Zscp
    class SyncedFolder < Vagrant.plugin("2", :synced_folder)
      def initialize(*args)
        super
        @logger = Log4r::Logger.new("vagrant::synced_folders::zscp")
      end

      def usable?(machine, raise_error=false)
        return true
      end

      def prepare(machine, folders, opts)
        # Nothing is necessary to do before VM boot.
      end

      def enable(machine, folders, opts)
        ssh_info = machine.ssh_info

        # Clean up every synced folder first
        foreach_folder(folders) do |folder_opts|
          ZscpHelper.cleanup(machine, ssh_info, folder_opts)
        end

        # Clean up every synced folder first
        foreach_folder(folders) do |folder_opts|
          ZscpHelper.scp(machine, ssh_info, folder_opts)
        end
      end

      def foreach_folder(folders, &block)
        sync_threads = []
        folders.each do |id, folder_opts|
          sync_threads << Thread.new(folder_opts) do |folder_opts|
            block.call(folder_opts)
          end
        end
        sync_threads.each do |t|
          t.join
        end
      end
    end
  end
end
